import os
from pyspark.sql import SparkSession
spark=SparkSession.builder\
      .appName("Hello spark")\
      .master("local") \
      .getOrCreate()

os.environ['JAVA_HOME']='C:\Program Files\Java\jdk1.8.0_261'

schema = StructType([
      StructField('Id',IntegerType(),True),
StructField('First',StringType(),True),
StructField('Last',StringType(),True),
StructField('Url',StringType(),True),
StructField('Published',StringType(),True),
StructField('Hits',IntegerType(),True),
StructField('Campign',ArryType(StringType()),True)
])
df=spark.read.schema().json('data Test/blogs.txt')