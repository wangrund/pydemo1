from pyspark.sql import SparkSession
from pyspark.sql.functions import col, expr, concat, countDistinct, to_timestamp, year,month,weekofyear,count
from pyspark.sql.types import StructType, ArrayType, StringType, StructField, IntegerType, BooleanType, FloatType
import os
spark=SparkSession.builder\
      .appName("Hello spark")\
      .master("local") \
      .getOrCreate()

os.environ['JAVA_HOME']='C:\Program Files\Java\jdk1.8.0_261'

fire_schema = StructType([StructField("CallNumber", IntegerType(), True),
                          StructField("UnitID", StringType(), True),
                          StructField("IncidentNumber", IntegerType(), True),
                          StructField("CallType", StringType(), True),
                          StructField("CallDate", StringType(), True),
                          StructField("WatchDate", StringType(), True),
                          StructField("CallFinalDisposition", StringType(), True),
                          StructField("AvailableDtTm", StringType(), True),
                          StructField("Address", StringType(), True),
                          StructField("City", StringType(), True),
                          StructField("Zipcode", IntegerType(), True),
                          StructField("Battalion", StringType(), True),
                          StructField("StationArea", StringType(), True),
                          StructField("Box", StringType(), True),
                          StructField("OriginalPriority", StringType(), True),
                          StructField("Priority", StringType(), True),
                          StructField("FinalPriority", IntegerType(), True),
                          StructField("ALSUnit", BooleanType(), True),
                          StructField("CallTypeGroup", StringType(), True),
                          StructField("NumAlarms", IntegerType(), True),
                          StructField("UnitType", StringType(), True),
                          StructField("UnitSequenceInCallDispatch", IntegerType(), True),
                          StructField("FirePreventionDistrict", StringType(), True),
                          StructField("SupervisorDistrict", StringType(), True),
                          StructField("Neighborhood", StringType(), True),
                          StructField("Location", StringType(), True),
                          StructField("RowID", StringType(), True),
                          StructField("Delay", FloatType(), True)
                          ]
                         )
df = spark\
      .read.option('header', True)\
      .schema(fire_schema)\
      .csv('C:/Users/Administrator/PycharmProjects/untitled3/data Test/sf-fire-calls.txt')

#1
# df.filter("CallType!='Medical Incident'")\
#     .select('IncidentNumber', 'AvailableDtTm', 'CallType')\
#     .show()
# #2计数
# df.select('CallType')\
#     .where(col('CallType').isNotNull())\
#     .groupBy('CallType')\
#     .count()\
#     .orderBy(col('count').desc())\
#     .show(truncate=False)
# #3去重
# df.filter(col('CallType').isNotNull())\
#     .select(col('CallType'))\
#     .distinct()\
#     .show()
# #4重命名
# df.withColumnRenamed('Delay', 'ResponseDelayedinMins') \
#     .where('ResponseDelayedinMins > 5') \
#     .select('ResponseDelayedinMins').show(truncate=False)
#5日期转换,增加列删除列
# cleaned_df = df.withColumn('IncidentDate', to_timestamp(col('CallDate'), 'MM/dd/yyyy')) \
#     .drop('CallDate') \
#     .withColumn("OnWatchDate", to_timestamp(col("WatchDate"), "MM/dd/yyyy")) \
#     .drop("WatchDate") \
#     .withColumn("AvailableDtTS", to_timestamp(col("AvailableDtTm"), "MM/dd/yyyy hh:mm:ss a")) \
#     .drop("AvailableDtTm")
# cleaned_df.select("IncidentDate", "OnWatchDate", "AvailableDtTS").show(5, True)\
#6日期转年函数
# cleaned_df.select(year('IncidentDate').alias('year')) \
#     .distinct() \
#     .orderBy(col('year').asc()) \
#     .show(truncate=False)
#7.
# df.filter(col('CallType').isNotNull())\
#     .groupBy(col('CallType'))\
#     .count()\
#     .orderBy(col('count').desc())\
#     .show()
#8
# cleaned_df.select(col('CallType'))\
#     .where(year('IncidentDate')==2018)\
#     .distinct()\
#     .show(truncate=False)
#9
# cleaned_df.select(month('IncidentDate').alias('month'))\
#     .where(year('IncidentDate')==2018)\
#     .orderBy(col('CallNumber').desc())\
#     .show(1)
10
cleaned_df.select('Neighborhood')\
    .where(year('IncidentDate')==2018)\
    .groupBy('Neighborhood')\
    .count()\
    .orderBy(col('count').desc())\
    .show(1)
11
cleaned_df.select('Neighborhood','Delay')\
    .where(year('IncidentDate')==2018)\
    .orderBy(col('Delay').desc())\
    .show(1)
12
cleaned_df.select(weekofyear('IncidentDate').alias('week'))\
    .where(year('IncidentDate')==2018)\
    .groupBy('week')\
    .count()\
    .orderBy(col('count').desc())\
    .show()



